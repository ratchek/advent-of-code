digits = File.read('input.txt').strip.split("")
sum = 0
digits.map! {|x| x.to_i}
digits.each_with_index do |x,i|
  sum+=x if x == digits[ (i+1) % digits.size ]
end
puts sum
