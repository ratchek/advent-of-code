sum = 0
File.readlines('input.txt').each do |line|
  digits = line.scan(/\d+/).map{ |d| d.to_i }
  sum += digits.max - digits.min
  end
puts sum
