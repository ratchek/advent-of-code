sum = 0
File.readlines('input.txt').each do |line|
  digits = line.scan(/\d+/).map{ |d| d.to_i }
  digit_pairs = digits.combination(2).to_a
  digit_pairs.each do |pair|
    if (pair[0] % pair[1] == 0   ||   pair[1] % pair[0] == 0) 
      sum+= pair.max/pair.min
      break
    end
  end

end
puts sum

